<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;
use borales\extensions\phoneInput\PhoneInputValidator;

/**
 * UserInfo 
 *
 * 
 *
 */
class UserInfo extends ActiveRecord{
{
   public function rules()
    {
        return [
            [['balance'], 'string']
        ];
    }
}
