<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;
use borales\extensions\phoneInput\PhoneInputValidator;

/**
 * Payment 
 *
 * 
 *
 */
class Payment extends ActiveRecord{
    public $phone;
    public $card_num;

    public function rules()
    {
        return [
            [['phone'], 'string'],
            [['phone'], PhoneInputValidator::className()],
        ];
    }
}