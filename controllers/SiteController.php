<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use mongosoft\soapserver\Action;
use app\models\Payment;
class SiteController extends Controller
{

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (empty($_GET)) {
            echo "Нет данных";
            die;
        }
        $this->checkForGET($_GET);
        $order = $this->GETtoOrder($_GET);
        $order_status = $this->checkForOrder($order);
        $payment = new Payment();
        return $this->render('pay',[
            'payment'=>$payment,
            'price_for_client'=>$_GET['amount'] / 100
        ]);
    }

    public function checkForGET($get){
        if (!isset($get['order_id'])) {
            echo "Не указан ID заказа";
            die;
        }
        if (!isset($get['amount'])) {
            echo "Не указана сумма";
            die;
        }
        if (!isset($get['service_id'])) {
            echo "Не указан service id";
            die;
        }
    }

    public function GETtoOrder($get){
        $order = [
            'service_id' => $get['service_id'],
            'order_id' => $get['order_id'],
            'amount' => $get['amount'],
        ];
        return $order;
    }

    public function checkForOrder($order){
        $client = new \mongosoft\soapclient\Client([
            'url' => 'http://client-test.loc/api/GetInformation',
        ]);
        $array = [
            'username' => 'isa',
            'password' => '123',
            'parameters' => [
                'order_id' => $order['order_id'],
            ],
            'serviceId' => $order['service_id']
        ];
        $array = json_encode($array);
        $response = $client->GetInformation($array);
        var_dump($response);
        die;
    }
}
