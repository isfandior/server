<?php 
namespace app\controllers;

use Yii;
use yii\web\Controller;
use mongosoft\soapserver\Action;
use app\models\Users;
class ApiController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => 'mongosoft\soapserver\Action',
                'serviceOptions' => [
                    'disableWsdlMode' => true,
                ]
            ]
        ];
    }
    /**
     * @param string $request
     * @return array
     * @soap
     */
    public function PerfomTransaction($request)
    {   
        $this->makePayment($request);
        return $this->PerfomTransactionResponse($request);
    }
   
    /**
     * @param string $request
     * @return array
     * @soap
     */
    public function GetInformation($request)
    {
       
        $user = $this->checkLogin($request);
        if ($user == false) {
            return $this->sendError("error login");
        }
        $data_to_return = [
            'errorMsg' => 'Ok',
            'status' => '0',
            'timeStamp' => Yii::$app->formatter->asTimestamp(date('Y-d-m h:i:s')),
            'parameters' => [
                'balance' => $user->balance,
                'username' => $user->username 
            ]
        ];
        return $data_to_return;
    }

    private function makePayment($request){
        $request = json_decode($request,true);
        $users = new Users();
        $user = $users->findOne($request['parameters']['user_id']);
        $user->balance = $user->balance - $request['parameters']['amount'];
        $user->save();
    }

    private function PerfomTransactionResponse($request){
        $request = json_decode($request,true);
        $users = new Users();
        $user = $users->findOne($request['parameters']['user_id']);
        $data_to_return = [
            'errorMsg' => 'Ok',
            'status' => '0',
            'timeStamp' => Yii::$app->formatter->asTimestamp(date('Y-d-m h:i:s')),
            'parameters' => [
                'balance' => $user->balance,
                'username' => $user->username 
            ]
        ];
        return $data_to_return;
    }

    private function checkLogin($request){
        $request = json_decode($request,true);
        $users = new Users();
        $user = $users->findOne($request['parameters']['user_id']);
        if ($user != null) {
            return $user;
            exit;
        }else{
            return false;
        }
    }

    private function sendError($error){
        $data_to_return = [
            'errorMsg' => $error,
            'status' => '0',
            'timeStamp' => Yii::$app->formatter->asTimestamp(date('Y-d-m h:i:s')),
        ];
        return $data_to_return;
    }


}
 ?>