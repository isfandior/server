<?php 
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use borales\extensions\phoneInput\PhoneInput;
 ?>
<div class="wrap-pay">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="pay-card">
					<h1>Пополнение кошелька на сумму: <?=number_format($price_for_client)?></h1>
					<div class="pay-form">
						<div class="col-md-6">
							<?php $form = ActiveForm::begin()?>
								<label>Оплата по номеру телефону</label>
								<?= $form->field($payment, 'phone')->label(false)->widget(PhoneInput::className(), ['jsOptions' => ['preferredCountries' => ['uz'],]]); ?>
								<?= Html::submitButton('Пополнить', ['class' => 'btn btn-success']) ?>
							<?php ActiveForm::end() ?>
						</div>
						<div class="col-md-6">
							<?php $form = ActiveForm::begin()?>
								<?= $form->field($payment, 'card_num')->label('Оплата карте')->textInput(['class'=>'phone-form form-control']); ?>
								<?= Html::submitButton('Пополнить', ['class' => 'btn btn-success']) ?>
							<?php ActiveForm::end() ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>